\  
enum SIZE {
    //% block="29*29"
    1,
    //% block="58*58"
    2
}

enum DIRECTIONS {
    //% block="μπροστά"
    1,
    //% block="πίσω"
    2
}

enum SPEED_OPTIONS {
    //% block="αργά"
    1250,
    //% block="κανονικά"
    1000,
    //% block="γρήγορα"
    750
}

enum TURN_OPTIONS {
    //% block="δεξιά"
    2,
    //% block="αριστερά"
    1
}

enum SHAKE_LEG_OPTIONS {
    //% block="δεξί"
    2,
    //% block="αριστερό"
    1
}

enum DANCES_3_ARGUMENTS {
    //% block="swing"
    0,
    //% block="tiptoeSwing"
    1,
    //% block="jitter"
    2,
    //% block="updown"
    3,
    //% block="ascendingTurn"
    4
}

enum DANCES_4_ARGUMENTS {
    //% block="moonwalker"
    0,
    //% block="crusaito"
    1,
    //% block="flapping"
    2,
}

enum OTTO_SONGS {
    //% block="connection"
    S_connection,
    //% block="disconnection"
    S_disconnection,
    //% block="button pushed"
    S_buttonPushed,
    //% block="mode 1"
    S_mode1,
    //% block="mode 2"
    S_mode2,
    //% block="mode 3"
    S_mode3,
    //% block="surprise"
    S_surprise,
    //% block="ohoh"
    S_OhOoh,
    //% block="ohoh v2"
    S_OhOoh2,
    //% block="cuddly"
    S_cuddly,
    //% block="sleeping"
    S_sleeping,
    //% block="happy"
    S_happy,
    //% block="super happy"
    S_superHappy,
    //% block="happy sort"
    S_happy_short,
    //% block="sad"
    S_sad,
    //% block="confused"
    S_confused,
    //% block="fart1"
    S_fart1,
    //% block="fart2"
    S_fart2,
    //% block="fart3"
    S_fart3
}

enum BUZZER_TONES_DURATION {
    //% block="Μισό"
    500,
    //% block="Τέταρτο"
    250,
    //% block="Όγδοο"
    125,
    //% block="Ολόκληρο"
    1000,
    //% block="Διπλό"
    2000,
    //% block="Στοπ"
    0,
}

enum BUZZER_TONES {
    //% block="Χαμηλό C/C3"
    131,
    //% block="Χαμηλό C#/C#3"
    139,
    //% block="Χαμηλό D/D3"
    147,
    //% block="Χαμηλό D#/D#3"
    156,
    //% block="Χαμηλό E/E3"
    165,
    //% block="Χαμηλό F/C3"
    175,
    //% block="Χαμηλό F#/F#3"
    185,
    //% block="Χαμηλό G/G3"
    196,
    //% block="Χαμηλό G#/G#3"
    208,
    //% block="Χαμηλό A/A3"
    220,
    //% block="Χαμηλό A#/A#3"
    233,
    //% block="Χαμηλό B/B3"
    247,
    //% block="Μεσαίο C/C4"SHAKE_LEG_OPTIONS
    277,
    //% block="Μεσαίο D/D4"
    294,
    //% block="Μεσαίο D#/D#4"
    311,
    //% block="Μεσαίο E/E4"
    330,
    //% block="Μεσαίο F/F4"
    349,
    //% block="Μεσαίο F#/F#4"
    370,
    //% block="Μεσαίο G/G4"
    392,
    //% block="Μεσαίο G#/G#4"
    415,
    //% block="Μεσαίο A/A4"
    440,
    //% block="Μεσαίο A#/A#4"
    466,
    //% block="Μεσαίο B/B4"
    494,
    //% block="Υψηλό C/C5"
    523,
    //% block="Υψηλό C#/C#5"
    554,
    //% block="Υψηλό D/D5"
    587,
    //% block="Υψηλό D#/D#5"
    622,
    //% block="Υψηλό E/E5"
    659,
    //% block="Υψηλό F/F5"
    698,
    //% block="Υψηλό F#/F#5"
    740,
    //% block="Υψηλό G/G5"
    784,
    //% block="Υψηλό G#/G#5"
    831,
    //% block="Υψηλό A/A5"
    880,
    //% block="Υψηλό A#/A#5"
    932,
    //% block="Υψηλό B/B5"
    988,
}

//% color="#2c763b" iconWidth=50 iconHeight=40
namespace alxR2 {
    //% block="Αρχικοποίηση Otto" blockType="command"
    export function initOtto(parameter: any, block: any) {
        if(Generator.board === 'arduinonano'){
            Generator.addInclude("Otto","#include <Otto.h>");
            Generator.addObject(`Otto` ,`Otto`,`alxOtto`);
            Generator.addSetup(`Otto_init`, `alxOtto.init(2, 3, 4, 5, true, 13);`);
            Generator.addSetup(`Otto_home`, `alxOtto.home();`);
        }
    }

    //% block="Κινήσου [STEPS] βήματα προς τα [DIRECTION] [SPEED]" blockType="command"
    //% STEPS.shadow="range" STEPS.defl="1" STEPS.params.max="5" STEPS.params.min="1"
    //% SPEED.shadow="dropdown" SPEED.options="SPEED_OPTIONS" SPEED.defl="SPEED_OPTIONS.1000"
    //% DIRECTION.shadow="dropdown" DIRECTION.options="DIRECTIONS" DIRECTION.defl="DIRECTIONS,1"
    export function ottoMove(parameter: any, block: any) {
        let speed = parameter.SPEED.code
        let steps = parameter.STEPS.code
        let direction = parameter.DIRECTION.code
        if direction==2
            direction=-1
        if(Generator.board === 'arduinonano'){
            Generator.addCode(`alxOtto.walk(${steps}, ${speed}, ${direction});`);
        }
    }

    //% block="Στρίψε προς τα [DIRECTION] [TIMES] φορές" blockType="command"
    //% DIRECTION.shadow="dropdown" DIRECTION.options="TURN_OPTIONS" DIRECTION.defl="TURN_OPTIONS,1"
    //% TIMES.shadow="range" TIMES.defl="1" TIMES.params.max="5" TIMES.params.min="1"
    export function ottoTurn(parameter: any, block: any) {
        let times = parameter.TIMES.code
        let direction = parameter.DIRECTION.code
        if direction==2
            direction=-1
        if(Generator.board === 'arduinonano'){
            Generator.addCode(`alxOtto.turn(${times}, 1000, ${direction});`);
        }
    }

    //% block="Σκύψε προς τα [DIRECTION] [TIMES] φορές" blockType="command"
    //% DIRECTION.shadow="dropdown" DIRECTION.options="TURN_OPTIONS" DIRECTION.defl="TURN_OPTIONS,1"
    //% TIMES.shadow="range" TIMES.defl="1" TIMES.params.max="5" TIMES.params.min="1"
    export function ottoBend(parameter: any, block: any) {
        let times = parameter.TIMES.code
        let direction = parameter.DIRECTION.code
        if direction==2
            direction=-1
        if(Generator.board === 'arduinonano'){
            Generator.addCode(`alxOtto.bend(${times}, 1000, ${direction});`);
        }
    }

    //% block="Κούνα το [DIRECTION] πόδι [TIMES] φορές" blockType="command"
    //% DIRECTION.shadow="dropdown" DIRECTION.options="SHAKE_LEG_OPTIONS" DIRECTION.defl="SHAKE_LEG_OPTIONS,1"
    //% TIMES.shadow="range" TIMES.defl="1" TIMES.params.max="5" TIMES.params.min="1"
    export function ottoShakeLeg(parameter: any, block: any) {
        let times = parameter.TIMES.code
        let direction = parameter.DIRECTION.code
        if(Generator.board === 'arduinonano'){
            if (direction==2)
                Generator.addCode(`alxOtto.shakeLeg(${times}, 1000, -1);`);
            else
                Generator.addCode(`alxOtto.shakeLeg(${times}, 1000, 1);`);
        }
    }

    //% block="Πήδησε [TIMES] φορές" blockType="command"
    //% TIMES.shadow="range" TIMES.defl="1" TIMES.params.max="5" TIMES.params.min="1"
    export function ottoJump(parameter: any, block: any) {
        let times = parameter.TIMES.code
        if(Generator.board === 'arduinonano'){
            Generator.addCode(`alxOtto.jump(${times}, 1000);`);
        }
    }

    //% block="Χόρεψε [DANCE] [TIMES] φορές προς τα [DIRECTION]" blockType="command"
    //% DANCE.shadow="dropdown" DANCE.options="DANCES_4_ARGUMENTS" DANCE.defl="DANCES_4_ARGUMENTS,0"
    //% TIMES.shadow="range" TIMES.defl="1" TIMES.params.max="5" TIMES.params.min="1"
    //% DIRECTION.shadow="dropdown" DIRECTION.options="TURN_OPTIONS" DIRECTION.defl="TURN_OPTIONS,1"
    export function ottoDance4(parameter: any, block: any) {
        let dance = parameter.DANCE.code
        let times = parameter.TIMES.code
        let direction = parameter.DIRECTION.code
        if direction==2
            direction=-1
        if(Generator.board === 'arduinonano'){
            if (dance==0)
                Generator.addCode(`alxOtto.moonwalker(${times}, 1000, 40, ${direction});`);
            else if (dance==1)
                Generator.addCode(`alxOtto.crusaito(${times}, 1000, 50, ${direction});`);
            else 
                Generator.addCode(`alxOtto.flapping(${times}, 1000, 30, ${direction});`);
        }
    }

    //% block="Χόρεψε [DANCE] [TIMES] φορές" blockType="command"
    //% DANCE.shadow="dropdown" DANCE.options="DANCES_3_ARGUMENTS" DANCE.defl="DANCES_3_ARGUMENTS,0"
    //% TIMES.shadow="range" TIMES.defl="1" TIMES.params.max="5" TIMES.params.min="1"
    export function ottoDance3(parameter: any, block: any) {
        let dance = parameter.DANCE.code
        let times = parameter.TIMES.code
        if(Generator.board === 'arduinonano'){
            if (dance==0)
                Generator.addCode(`alxOtto.swing(${times}, 1000, 50);`);
            else if (dance==1)
                Generator.addCode(`alxOtto.tiptoeSwing(${times}, 1000, 50);`);
            else if (dance==2)
                Generator.addCode(`alxOtto.jitter(${times}, 1000, 25);`);
            else if (dance==3)
                Generator.addCode(`alxOtto.updown(${times}, 1000, 70);`);
            else
                Generator.addCode(`alxOtto.ascendingTurn(${times}, 1000, 15);`);
        }
    }

    //% block="Παίξε τραγούδι [SONG]" blockType="command"
    //% SONG.shadow="dropdown" SONG.options="OTTO_SONGS" SONG.defl="OTTO_SONGS,S_connection"
    export function ottoSing(parameter: any, block: any) {
        let song = parameter.SONG.code
        if(Generator.board === 'arduinonano'){
            Generator.addCode(`alxOtto.sing(${song});`);
        }
    }

    //% block="Διάβασε απόσταση σε εκατοστά" blockType="reporter"
    export function getDistance(parameter: any, block: any) {
        if(Generator.board === 'arduinonano'){
            Generator.addInclude("DFRobot_URM","#include <DFRobot_URM10.h>");
            Generator.addObject(`DFRobot_URM` ,`DFRobot_URM10`,`alx_distance`);
            Generator.addCode(`(alx_distance.getDistanceCM(8, 9))`);
        }
    }

    function replace(str :string) {
        return str.replace("+", "");
    }
}

